<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function utama()
    {
        return view('Home');
    }

    public function tabel()
    {
        return view('halaman.table');
    }
}
